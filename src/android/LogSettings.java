/*
* Migros-Genossenschafts-Bund
*/
package ch.migros.plugin.printer;

/**
 * Hilfsklasse für das Ein- und Ausschalten
 * der LogCat Protokolle.
 * 
 * Dies ist eine Optimierung der Laufzeitumgebung.
 * In einer produktiven Umgebung sollte auf die LogCat Ausgabe
 * verzichtet werden. 
 * 
 * Die Protokollierung ist per Default ausgeschaltet.
 * 
 * Hier ein Beispiel der Codierung:
 * 
 * if (LogSetting.enabled)
 * 	  Log.v("myTag","myLogText");
 *
 * @author FMaffeni
 */
public class LogSettings {
	public static volatile boolean enabled=false;
}
