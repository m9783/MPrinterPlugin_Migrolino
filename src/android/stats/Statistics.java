/*
* Migros-Genossenschafts-Bund
*/
package ch.migros.plugin.printer.stats;

import java.util.HashMap;
import java.util.Map;

/**
 * @author FMaffeni
 */
public class Statistics {

	private static Map<String, Statistics> s_statMap=new HashMap<String, Statistics>();
	private String _printerName=null;
	private int _printedCount=0;
	private int _failedCount=0;
	
	/**
	 * Constructor
	 * @param printerName2
	 */
	public Statistics(String printerName) {
		_printerName=printerName;
	}

	public static void incPrinted(String printerName, int cnt) {
		Statistics stats=getPrinterStat(printerName);
		stats._printedCount=stats._printedCount+cnt;
		store();
	}

	public static void incFailed(String printerName, int cnt) {
		Statistics stats=getPrinterStat(printerName);
		stats._failedCount=stats._failedCount+cnt;
		store();
	}

	/**
	 * 
	 */
	private static void store() {
		// TODO persist this values later
		
	}

	/**
	 * @param printerName2
	 * @return
	 */
	private static Statistics getPrinterStat(String printerName) {
		Statistics stat=s_statMap.get(printerName);
		if (stat==null) {
			stat=new Statistics(printerName);
			s_statMap.put(printerName, stat);
		}
		return stat;
	}
	
	
}
