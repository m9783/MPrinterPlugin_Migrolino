package ch.migros.plugin.printer;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.util.Log;

import ch.migros.plugin.printer.network.Connection;
import ch.migros.plugin.printer.spooler.Destination;
import ch.migros.plugin.printer.spooler.PrintLanguageE;
import ch.migros.plugin.printer.spooler.PrintSpooler;
import ch.migros.plugin.printer.spooler.PrinterCharacteristics;

public class Plugin_Printer extends CordovaPlugin {

	private static final String PRINTER_MODEL_UNKNOWN = "Printermodel unknown";
	private static final String HOSTNAME_PART_MOBILE_PRINTER_PREFIX = "mp";
	private static final String HOSTNAME_PART_PRINTER_MODEL_Q220 = "q220";
	private static final String PRINTER_MODEL_ZEBRA_QLN220 = "Zebra QLn220";
	private static final String LOGTAG = Plugin_Printer.class.getName();
	private static PrintSpooler s_spooler=null;

	/* ****************************************************************************************************
	 * CORDOVA SCHNITTSTELLE 
	 * ****************************************************************************************************/
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		if (LogSettings.enabled)
			Log.v("PrinterPlugin", "initialize");
		initPrintSpooler();
	}

	/**
	 * Entry-Point für die Action Auslösung dieses Plugins.
	 */
	@Override
	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
		if (LogSettings.enabled)
			Log.v("PrinterPlugin", "execute(\""+action+"\") called");
		boolean ret=false;

		if ("printLabel".equals(action))
			ret=printLabel(action,args,callbackContext);
		else if ("setCurrentPrinter".equals(action))
			ret=setCurrentPrinter(action,args,callbackContext);
		else if ("getPrinterLanguage".equals(action))
			ret=getPrinterLanguage(action,args,callbackContext);
		else if ("stopPrintSpooler".equals(action))
			ret=stopPrintSpooler(action,args,callbackContext);
		else if ("startPrintSpooler".equals(action))
			ret=startPrintSpooler(action,args,callbackContext);
		else if ("resetAllPrintQueues".equals(action))
			ret=resetAllPrintQueues(action,args,callbackContext);
		else if ("resetPrintQueueForDest".equals(action))
			ret=resetPrintQueueForDest(action,args,callbackContext);
		else if ("registerProcessingCallback".equals(action))
			ret=registerProcessingCallback(action,args,callbackContext);
		else if ("unregisterProcessingCallback".equals(action))
			ret=unregisterProcessingCallback(action,args,callbackContext);
		else if ("setLogging".equals(action))
			ret=setLogging(action,args,callbackContext);
		else if ("getCurrentPrinter".equals(action))    //added@20160825/fma
			ret=getCurrentPrinter(action,args,callbackContext);
		else if ("removeCurrentPrinter".equals(action)) //added@20160825/fma
			ret=removeCurrentPrinter(action,args,callbackContext);

		return ret;
	}

	/**
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return
	 */
	private boolean setLogging(String action, JSONArray args,
			CallbackContext callbackContext) {
		boolean loggingOn=false;
		try {
			loggingOn=args.getInt(0)==1;
		} catch (Exception silent) {}
		LogSettings.enabled=loggingOn;
		Log.v(LOGTAG,"setLogging on LogCat="+loggingOn);
		callbackContext.success();
		return true;
	}

	/**
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return
	 */
	private boolean unregisterProcessingCallback(String action, JSONArray args,
			CallbackContext callbackContext) {
		s_spooler.setCordovaCallback(null, null);
		callbackContext.success();
		return true;
	}

	/**
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return
	 */
	private boolean registerProcessingCallback(String action, JSONArray args,
			CallbackContext callbackContext) {
		s_spooler.setCordovaCallback(cordova, callbackContext);

		PluginResult result = new PluginResult(PluginResult.Status.OK);
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);
		return true;
	}

	/**
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return 
	 */
	private boolean resetPrintQueueForDest(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {

		Destination dest=getCheckedCurrentDestination(callbackContext,"resetPrintQueueForDest");
		if (dest==null)
			return true;

		String hostName=args.getString(0);
		int    port=args.getInt(1);

		s_spooler.resetDestiationQueue(hostName,port);
		callbackContext.success();
		return true;
	}

	/**
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return 
	 */
	private boolean resetAllPrintQueues(String action, JSONArray args,
			CallbackContext callbackContext) {
		s_spooler.resetDestinationQueues();
		callbackContext.success();
		return true;
	}

	/* ****************************************************************************************************
	 * NATIVE PRINTER SPOOLER SCHNITTSTELLE 
	 * ****************************************************************************************************/
	private void initPrintSpooler() {
		s_spooler=PrintSpooler.getInstance();
	}

	/**
	 * Setzt den aktuellen Drucker in den Druckerspooler.
	 * Alle nachfolgenden Ausdrucke werden diesem Drucker weitergelietet.
	 * 
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return 
	 * @throws JSONException
	 */
	private boolean setCurrentPrinter(String action, JSONArray args, final CallbackContext callbackContext)  throws JSONException {
		s_spooler=PrintSpooler.getInstance();
		PrinterCharacteristics pc=new PrinterCharacteristics();
		String hostName=args.getString(0);
		int    port=args.getInt(1);
		String printerModel=extractPrinterModelFromHostName(hostName);

		pc.setNetworkName(hostName);
		pc.setPort(Integer.valueOf(port));
		pc.setPrinterModel(printerModel);

		//Die richtige Drucksprache f�r den adressierten Drucker setzen
		PrintLanguageE printLang=PrintLanguageE.undefined;
		if (PRINTER_MODEL_ZEBRA_QLN220.equals(printerModel)) 
			printLang=PrintLanguageE.ZPL;

		pc.setPrintLanguage(printLang);
		s_spooler.setCurrentPrinter(pc);
		callbackContext.success();
		return true;
	}

	/**
	 * Entfernt den aktuellen Drucker aus dem Druckerspooler.
	 * Um erneut ausdrucken zu können, muss wieder ein aktueller Drucker gesetzt werden.
	 * added@20160825/fma
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return
	 * @throws JSONException
	 */
	private boolean removeCurrentPrinter(String action, JSONArray args, final CallbackContext callbackContext)  throws JSONException {
		s_spooler=PrintSpooler.getInstance();
		Destination currDest=s_spooler.getCurrentDest();
		if (currDest!=null) {
			PrinterCharacteristics pc = currDest.getPrinterCharacteristics();
			s_spooler.resetDestiationQueue(pc.getNetworkName(), pc.getPort());
			s_spooler.removeCurrentPrinter();
		} else {
			Log.d(LOGTAG,"removeCurrentPrinter. no current dest to remove.");
		}
		callbackContext.success();
		return true;
	}
	/**
	 * Extrahiert den Drucker Modellnamen aus
	 * dem gesetzen Hostnamen.
	 * 
	 * Die Definition des Hostnames ist wie folgt:
	 * Hostname: mpdddd-ggggggggg
	 * mp        =fixer String f�r Erkennung im Netzwerk
	 * dddd      =Drucker Typ (z.B. q220)
	 * -         =fixer Delimiter
	 * ggggggggg = gmpos-id 9stelling, nur numerischer Teil
	 * @return
	 */
	@SuppressLint("DefaultLocale")
	private String extractPrinterModelFromHostName(String hostName) {
		String printerModel=PRINTER_MODEL_UNKNOWN;
		if (hostName==null)
			return printerModel;

		String tHostName=hostName.toLowerCase();
		if (tHostName.startsWith(HOSTNAME_PART_MOBILE_PRINTER_PREFIX)
				&& tHostName.length()>5) {
			String pModelStr=tHostName.substring(2,6);
			if (HOSTNAME_PART_PRINTER_MODEL_Q220.equals(pModelStr))
				printerModel=PRINTER_MODEL_ZEBRA_QLN220;
		}
		return printerModel;
	}

	/**
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return 
	 */
	private boolean startPrintSpooler(String action, JSONArray args,
			CallbackContext callbackContext) {
		s_spooler.start();
		callbackContext.success();
		return true;
	}

	/**
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return 
	 */
	private boolean stopPrintSpooler(String action, JSONArray args,
			CallbackContext callbackContext) {
		s_spooler.stop();
		callbackContext.success();
		return true;
	}

	/**
	 * Druckt die Etikette über den nativen Druckerspooler. Als destination wird
	 * der letzt gesetzte Drucker verwendet.
	 * 
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return 
	 * @throws JSONException
	 */
	private boolean printLabel(String action, JSONArray args, final CallbackContext callbackContext)  throws JSONException {
		Destination dest=getCheckedCurrentDestination(callbackContext,"printLabel");
		if (dest==null)
			return true;

		String label=args.getString(0);

		//Standardmässig wird nicht auf eine Antwort des Druckers gewartet
		//Wenn gewartet wird, dann ist der Default Timeout auf 1500 ms gesetzt.
		//Maximal kann der Timeout auf 45 Sekunden gesetzt werden.
		boolean answerExcepted=false;
		int waitForAnswerMs=1500;
		final int maxWaitMs=45000;

		if (args.length()>2) { //Idx0: Etikette, Idx1: Quantity, Idx2:answerExcepted, Idx3: waitForAnswerMs
			try {
				answerExcepted = "true".equalsIgnoreCase(args.getString(2));
			} catch (Exception silent) {
			}
			try {
				waitForAnswerMs = args.getInt(3);
				if (waitForAnswerMs > maxWaitMs)
					waitForAnswerMs = maxWaitMs;
			} catch (Exception silent) {
			}
		}

		int    cnt=args.getInt(1);
		Charset utf8charset = Charset.forName("UTF-8");
		Charset iso88591charset = Charset.forName("ISO-8859-1");
		ByteBuffer inputBuffer = ByteBuffer.wrap(label.getBytes());

		// Eingabebuffer decodieren
		CharBuffer charBuf=utf8charset.decode(inputBuffer);
		ByteBuffer byteBuf = iso88591charset.encode(charBuf);
		
		s_spooler=PrintSpooler.getInstance();
		/**
		 * Ist eine Antwort vom Drucker gewünscht, dann muss ein callback Objekt
		 * für die Rückgabe der Antwort erzeugt werden.
		 */
		Destination.PrinterResponseI asyncPrinterResponse=null;

		if (answerExcepted) {
			asyncPrinterResponse=new Destination.PrinterResponseI() {
				@Override
				public void responseReceived(final Connection.RetVal printerResponse) {
					cordova.getActivity().runOnUiThread(new Runnable() {
						public void run() {
							PluginResult result = null;
							JSONArray resultArr = new JSONArray();
							resultArr.put(printerResponse.ok);
							String resTxt=printerResponse.receivedFromPrinter==null
									     ? ""
									     : printerResponse.receivedFromPrinter;
							resultArr.put(resTxt);
							result = new PluginResult(PluginResult.Status.OK, resultArr);
							result.setKeepCallback(false);
							callbackContext.sendPluginResult(result);
						}
					});
				}
			};
		}
		s_spooler.printLabel(byteBuf.array(), cnt,answerExcepted,waitForAnswerMs,asyncPrinterResponse);

		if (!answerExcepted)
			callbackContext.success();

		return true;
	}

	/**
	 * Gibt die Drucker Kommando Sprache zurück, sodass
	 * die Fiori App die Etiketten in der richtigen Sprache
	 * erstellen kann.
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return 
	 */
	private boolean getPrinterLanguage(String action, JSONArray args,
			final CallbackContext callbackContext) {
		final Destination dest = getCheckedCurrentDestination(callbackContext,"getPrinterLanguage");
		if (dest!=null) {
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					PrintLanguageE printLanguage=PrintLanguageE.undefined;
					PluginResult result=null;
					printLanguage=dest.getPrinterCharacteristics().getPrintLanguage();
					JSONArray resultArr=new JSONArray();
					resultArr.put(printLanguage.ordinal());
					resultArr.put(printLanguage.toString());
					result = new PluginResult(PluginResult.Status.OK, resultArr);
					result.setKeepCallback(false);
					callbackContext.sendPluginResult(result);
				}
			});
		}
		return true;
	}

	/**
	 * Gibt den aktuell gesetzten Drucker zurück.
	 * added@20160825/fma
	 * @param action
	 * @param args
	 * @param callbackContext
	 * @return
	 */
	private boolean getCurrentPrinter(String action, JSONArray args,
									   final CallbackContext callbackContext) {

		final Destination dest = getCheckedCurrentDestination(callbackContext,"getCurrentPrinter");
		if (dest!=null) {
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					PrintLanguageE printLanguage=PrintLanguageE.undefined;
					PluginResult result=null;
					String networkName=dest.getPrinterCharacteristics().getNetworkName();
					int port=dest.getPrinterCharacteristics().getPort();
					String printerModel=dest.getPrinterCharacteristics().getPrinterModel();
					printLanguage=dest.getPrinterCharacteristics().getPrintLanguage();

					JSONArray resultArr=new JSONArray();

					resultArr.put(networkName);
					resultArr.put(port);
					resultArr.put(printerModel);

					resultArr.put(printLanguage.ordinal());
					resultArr.put(printLanguage.toString());

					result = new PluginResult(PluginResult.Status.OK, resultArr);
					result.setKeepCallback(false);
					callbackContext.sendPluginResult(result);
				}
			});
		}
		return true;
	}

	/**
	 * Gibt den Destination Objekt vom Spooler zurück.
	 * Falls noch keine Destination gesetzt ist, wird die Fiori App
	 * mit dem Fehler benachrichtigt, dass noch keine Destination gesetzt wurde.
	 * 
	 * @param callbackContext
	 * @return
	 */
	private Destination getCheckedCurrentDestination(final CallbackContext callbackContext, final String methodText) {
		Destination dest = PrintSpooler.getInstance().getCurrentDest();
		if (dest!=null)
			return dest;

		cordova.getActivity().runOnUiThread(new Runnable() {
			public void run() {
				PluginResult result=null;
				result = new PluginResult(PluginResult.Status.ERROR, "Print destination not set. Call 'setCurrentPrinter' before using '"+methodText+"'");
				result.setKeepCallback(false);
				callbackContext.sendPluginResult(result);
			};
		});
		return null;
	}
}