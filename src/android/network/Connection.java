/*
 * Migros-Genossenschafts-Bund
 */
package ch.migros.plugin.printer.network;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import android.util.Log;
import ch.migros.plugin.printer.LogSettings;

/**
 * Die Connection Klasse baut die Netzwerkverbindung via TCP/IP Sockets
 * zum Drucker auf und wieder ab. Über das zur Verfügung gestellte Socket
 * werden die Etiketten-Druckbefehle als bytestream weitergeleitet.
 *
 * 20160826/fma: Die Connection Klasse wird um die bidirektionale Kommunikation
 *               erweitert, sodass Returnwerte auf Kommandobefehle vom Drucker
 *               zur Applikation zurückgesendet werden können.
 *
 * @author FMaffeni
 */
public class Connection {
	private static final String LOGTAG = Connection.class.getName();

	private static final int SOCKET_CONNECT_TIMEOUT_MS = 500;
	
	private String     _host=null;
	private int        _port=0;
	private Socket     _socket=null;
	private Exception  _lastEx=null;
	private ByteBuffer _buffer=ByteBuffer.allocate(32*1024);
	private boolean    _isSending=false;
	private InetAddress _printerInetAddress=null;

	/**
	 * Constructor erstellt eine neue Netzwerkverbindung
	 * zur angegebenen Adresse und Port
	 * 
	 * @param host DNS name oder ip-Adresse in String format
	 * @param port portnummer des Druckdienstes. idR. 6101 oder 9100
	 */
	public Connection(String host, int port) {
		if (host==null || "".equals(host.trim()))
			throw new RuntimeException(String.format("passed host '%s' is not valid",host)); 

		if (port <0 || port>65535)
			throw new RuntimeException(String.format("passed port %i is not valid",port)); 
		_host=host;
		_port=port;
		if (LogSettings.enabled)
			Log.v(LOGTAG,"Connection constructor for destination: " +host + ":"+port);
		_printerInetAddress=nsLookup();
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return _host;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return _port;
	}

	/**
	 * Führt ein DNS Lookup aus. 
	 * Wenn der Hostname aufgelöst werden kann, so wird
	 * das ermittelte InetAdress Objekt zurückgegeben.
	 * 
	 * @return
	 */
	public InetAddress nsLookup() {
		InetAddress inetAddress=null;
		try {
			inetAddress=InetAddress.getByName(_host);
			if (LogSettings.enabled)
				Log.v(LOGTAG,"nslookup for host: "+ _host 
					+ " returned hostAddress:"
					+inetAddress.getHostAddress() 
					+ ", hostName:" +inetAddress.getHostName()
					+ ", hostCanonicalName:" +inetAddress.getCanonicalHostName());
		} catch (UnknownHostException e) {
			if (LogSettings.enabled)
				Log.v(LOGTAG,"nslookup for host: "+ _host + " did fail with exception " + e.toString());		
		}
		return inetAddress;
	}
	
	/**
	 * Führt einen Ping auf die gesetzte Ip-Adresse aus.
	 * Somit wird ermittelt ob der Drucker erreichbar ist oder nicht.
	 * @return
	 */
	public boolean ping() {
		boolean reached=false;
		try {
			if (_printerInetAddress==null)
				_printerInetAddress=nsLookup();
			reached=_printerInetAddress.isReachable(SOCKET_CONNECT_TIMEOUT_MS);
			if (LogSettings.enabled)
				Log.v(LOGTAG,"ping the inetAddress " + _printerInetAddress.getHostAddress() + " host reached="+reached);
		} catch (IOException e) {
			if (LogSettings.enabled)
				Log.v(LOGTAG,"ping the inetAddress: "+ _printerInetAddress.getHostAddress() + " did fail with exception " + e.toString());
		}
		return reached;
	}
	
	/**
	 * Fügt die Übergebene ByteArray zum Druckbuffer
	 * mit Anzahl=1;
	 * @param byteArray
	 * @return
	 */
	public boolean addToPrintBuffer(byte[] byteArray) {
		return addToPrintBuffer(byteArray,1);
	}
	
	/**
	 * Fügt die Übergebene byte Array in den Connection
	 * buffer. Der Connection bytebuffer wird so lange
	 * gehalten bis die send() Methode aufgerufen wird
	 * 
	 * Es wird sichergestellt, dass die Anzahl copies
	 * innerhalb eines sendebuffers zum Drucker gesendet
	 * wird.
	 * 
	 * @param byteArray
	 * @param copies
	 * @return
	 */
	public boolean addToPrintBuffer(byte[] byteArray, int copies) {
		boolean added=false;
		synchronized(_buffer) {
			if (LogSettings.enabled)
				Log.v(LOGTAG,"Connection add bytes to printer buffer: " + String.valueOf(byteArray));
			if (!_isSending)  {
				for (int i = 0; i < copies; i++) {
					_buffer.put(byteArray);
				}
				added=true;
			}
		}
		return added;
	}

	/**
	 * Gibt den aktuellen Sendezustand zurück.
	 * True bedeutet, dass die Buffer Daten aktuell
	 * über das Netzwerk Outputstream versendet werden
	 * und deshalb der interne Buffer nicht verändert werden darf.
	 *  
	 * @return the isSending
	 */
	public boolean isSending() {
		return _isSending;
	}

	/**
	 * Gibt die letzte gefangene Ausnahme aus, die während
	 * des letzten Sendevorganges aufgetreten ist.
	 * Dieser wert ist null wenn die Daten fehlerfrei
	 * übermittelt werden konnten.
	 * 
	 * @return
	 */
	public Exception getLastException() {
		return _lastEx;
	}

	/**
	 * Der aktuelle Buffer wird zum Drucker gesendet.
	 * @param waitForAnswerMs
	 *
	 * @return Statuswert. true=Druckbuffer vollständig gesendet. false=Fehler aufgetreten. siehe  @see _lastEx
	 */
	public RetVal send(boolean answerExcepted, int waitForAnswerMs) {

		RetVal ret=new RetVal();

		_lastEx=null;
		try {
			if (LogSettings.enabled) {
				Log.v(LOGTAG,"--> PRINT LABEL START");
				Log.v(LOGTAG,"Connection open socket destination: " +_host + ":"+_port);
			}

			//öffne einen neuen Netzwerk-Socket, falls keines offen ist.
			if (_socket==null) {
				_socket=new Socket();
				_socket.connect(new InetSocketAddress(_host,_port),SOCKET_CONNECT_TIMEOUT_MS);
			}
			int f=0;
			int t=_buffer.position();

			synchronized (_buffer) {
				_isSending=true;
			}
			if (LogSettings.enabled)
				Log.v(LOGTAG,"Connection send data (bytecount="+(t-f)
						+" for destination: " +_host + ":"+_port
						+" bufferContent: "+new String(_buffer.array(), f, t));

			_socket.getOutputStream().write(_buffer.array(), f, t);
			_socket.getOutputStream().flush();
			InputStream is=null;
			final int RECV_BUF_SIZE=512;
			byte[] buf=new byte[RECV_BUF_SIZE];

			int len=buf.length;
			int off=0;
			if (answerExcepted) {
				try {
					Thread.sleep(waitForAnswerMs);
				} catch (InterruptedException silent) {}

				is=new BufferedInputStream(_socket.getInputStream());
				do {
					if (is.available()>0) {
						len=buf.length-off;
						//Buffer vergrössern, falls ein overflow stattfinden würde.
						//Auf einen max Check wird verzichtet, da normalerweise vom
						//Drucker keine grossen Datenmengen zurückgesendet werden.
						//Im Ausnahmefall würde eine Java Memory Exception geworfen.
						if (len<1) {
							buf= Arrays.copyOf(buf,buf.length+RECV_BUF_SIZE);
						}
						len=is.read(buf,off,len);
						off+=len;
					}
				} while (is.available()>0);
				System.out.println("buf: "+ new String(buf,0,off));
				ret.receivedFromPrinter=new String(buf,0,off);
			}
			ret.ok=true;
			if (LogSettings.enabled)
				Log.v(LOGTAG,"--> PRINT LABEL END OK");
		} catch (IOException e) {
			_lastEx=e;
			if (LogSettings.enabled)
				Log.v(LOGTAG,"--> PRINT LABEL END FAILED. Exception was: "+e);
			//bei Fehlern, vorhanderer Netzwerksocket schliessen, sodass beim nächsten Durchlauf ein neues eröffnet wird
			close();
		}
		finally {
			_buffer.clear(); //Der Sendebuffer wird auf jedenfall geleert um so ein Aufschaukeln der Etiketten bei nicht vorhandener Destination (Neztwerkprobleme, Drucker aus etc.) zu vermeiden
			close();
		}

		return ret;
	}

	
	/**
	 * Connection Shutdown erwingt die Schliessung
	 * des Sockets unabhängig vom derzeitigen Zustand
	 */
	public void close() {
		if (LogSettings.enabled)
			Log.v(LOGTAG,"Connection close socket destination: " +_host + ":"+_port);
		try {
			if (_socket!=null)
			_socket.close();
		} catch (Exception silent) {};
		_socket=null;
		synchronized (_buffer) {
			_isSending=false;
		}
	}

	/**
	 * Gibt zurück ob die Connection die ip-Adresse des hostnamens
	 * bereits über nslookup ermittelt hat.
	 * @return
	 */
	public boolean isHostNameResolved() {
		return _printerInetAddress!=null;
	}

	public static class RetVal {
		public boolean ok=false;
		public String  receivedFromPrinter=null;
	}

}
