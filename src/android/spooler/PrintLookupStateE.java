/*
* Migros-Genossenschafts-Bund
*/
package ch.migros.plugin.printer.spooler;

/**
 * @author FMaffeni
 */
public enum PrintLookupStateE {
	 undefined
	,dnsLookupFailed
	,dnsLookupOkPingFailed
	,dnsLookupOkPingOk
}
