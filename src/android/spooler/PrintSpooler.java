/*
 * Migros-Genossenschafts-Bund
 */
package ch.migros.plugin.printer.spooler;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

import android.util.Log;
import ch.migros.plugin.printer.LogSettings;
import ch.migros.plugin.printer.network.Connection;

/**
 * Der PrintSpooler bietet die öffentliche Schnittstelle für den Fiori Plugin
 * Etikettendruck.
 * 
 * Der Spooler hält Destinationen welche wiederum die Netzwerkkonnektivität halten.
 * 
 * Bevor Etiketten ausgedruckt werden können, muss zwingend der Destination Drucker mit
 * setCurrentPrinter(PrinterCharacteristics pc) gesetzt werden.
 * 
 * Alle darauffolgenden Druckaufträge werden in diese Destination geschrieben. Diese
 * bleibt solange gültig, bis eine neue gesetzt wird oder die Applikation und demzufolge
 * auch das Plugin neu gestartet wird.
 * 
 * 
 *
 * @author FMaffeni
 */
public class PrintSpooler implements Runnable, DestinationStateChangeListenerI {
	
	private static final String PRINTER_PLUGIN_CALLBACK_PENDING_PRINT_JOBS = "PrinterPluginCallback_PendingPrintJobs";
	private static final String PRINTER_PLUGIN_CALLBACK_STATE_CHANGE = "PrinterPluginCallback_StateChange";

	private static final String HOST_PORT_SEP_CHAR = ":";
	private static final String LOGTAG = PrintSpooler.class.getName();
	private Map<String, Destination> _destinationMap=new HashMap<String, Destination>();
	private PrinterCharacteristics _currPrinter=null;
	private boolean _running;
	private Thread _thread;
	private static final int DESTINATION_QUEUE_CLEANUP_INTERVAL_MS=30000; //Prüfintervall
	private static final long QUEUE_CHECK_INTERVAL_TS = 60000; //Verweildauer von Druckaufträgen in der Queue bis die Fiori App über herumliegenden Etiketten informiert wird
	private static final long QUEUE_MAX_LIVETIME_MS = 30000;  //Maximale Lebensdauer für eine Destination/Printqueue wenn keine Druckaufträge mehr darin enthalten sind
	private volatile static PrintSpooler s_instance=null;
	private CordovaInterface _cordovaInterface=null;
	private CallbackContext  _cordovaCallbackContext=null;
	
	/**
	 * @param cordovaCallbackContext the cordovaCallbackContext to set
	 */
	public void setCordovaCallback(CordovaInterface cordovaInterface, CallbackContext cordovaCallbackContext) {
		_cordovaInterface=cordovaInterface;
		_cordovaCallbackContext=cordovaCallbackContext;
	}
	
	//Statische Intanzinitialisierung
	static {
		init();
	}
	
	/**
	 * singleton initialisieren.
	 * @return singleton
	 */
	private static void init() {
		s_instance= new PrintSpooler();
		s_instance.start();
	}
	
	/**
	 * Singleton
	 */
	private PrintSpooler() {
	}
	
	/**
	 * Die im <b>printLabel</b> Übergebene Etikette wird
	 * in die PrintQueue des aktuell gewählten Druckers eingetragen.
	 * Die Verarbeitung erfolgt asynchron. Diese Funktion ist nonblocking
	 * und kann vom UI Thread ohne Verzögerungen aufgerufen werden.
	 * 
	 * @param label    Die in der Druckersprache formatierte Etikette
	 * @param quantity Auszudruckende Anzahl
	 * @param answerExcepted Soll auf eine Antwort des Druckers gewartet werden?
	 * @param waitForAnswerMs Wie lange soll auf die Antwort gewartet werden
	 */
	public void printLabel(byte[] label, int quantity, boolean answerExcepted, int waitForAnswerMs, Destination.PrinterResponseI asyncResponseCallback) {
		if (_currPrinter==null) {
			//Sollte nicht vorkommen, da der Check bereits in der Plugin_Printer Klasse durchgef�hrt wird
			throw new RuntimeException("no current printer destination set.");
		}
		
		Destination dest=getCurrentDest();
		LabelDef ld=new LabelDef(_currPrinter,label,quantity,answerExcepted,waitForAnswerMs,asyncResponseCallback);
		dest.getLabelQueue().add(ld);
		dest.fireAddEvent();
	}
	
	/**
	 * Setzt den akutellen Printer für die nachfolgenden Druckaufträge
	 * @param printer
	 */
	public void setCurrentPrinter(PrinterCharacteristics printer) {
		_currPrinter=printer;
		//initialisieren der Destination sofort forcieren um somit
		//die Destinationsaufl�sung zu starten sodass die 
		//DNS Aufl�sung gemacht und er Printer gesucht wird.
		//Via Callback, welches bereits gesetzt sein muss, wird
		//dann der Zustand dem GUI zur�ckgeliefert.
		getCurrentDest();
	}

	/**
	 * Entfernt den aktuellen Printer vom Spooler, sodass für neue Ausdrucke
	 * dieser erneut gesetzt werden muss. Diese Funktion wird dazu verwendet,
	 * die Zuweisung beim Verlassen der Label Druck Applikation zu lösen.
	 */
	public void removeCurrentPrinter() {
		Destination dest=getCurrentDest();
		String currDestKey=currentDestKey();
		dest.close();
 		_destinationMap.remove(currDestKey);
		_currPrinter=null;
	}

	/**
	 * Gibt die aktuelle Druckdestination zurück. Ist die Destination
	 * noch nicht zum Drucken verwendet worden, erzeugt diese Methode
	 * ein neues Destinationsobjekt und weist eine neue Druck-Queue zu.
	 * @return
	 */
	public Destination getCurrentDest() {
		Destination dest=_destinationMap.get(currentDestKey());
		if (dest==null && _currPrinter!=null) {
			dest=new Destination(_currPrinter,this);
			_destinationMap.put(currentDestKey(), dest);
		}
		return dest;
	}

	/**
	 * Erstellt den Map Schlüssel für die aktuell gesetzte
	 * Destination. Ist diese noch nicht gesetzt, wird null
	 * zurückgegeben.
	 * @return
	 */
	private String currentDestKey() {
		String destKey=_currPrinter!=null
				      ? destKeyFor(_currPrinter.getNetworkName(),_currPrinter.getPort())
				      : null;
		return destKey;
	}

	/**
	 * Erstellt den Schlüssel für die Destination Map
	 * @param hostName
	 * @param port
	 * @return
	 */
	private String destKeyFor(String hostName, int port) {
		return hostName+HOST_PORT_SEP_CHAR+port;
	}

	/**
	 * Worker thread des PrintSpoolers.
	 * 
	 * Checkt periodisch den Inhalt aller aktuellen PrintQueues
	 * und generiert wo nötig einen Fiori-Callback mit der Meldung
	 * über die hängen gebliebenen Etiketten und Destinationen.
	 *  
	 * Queues, welche längere Zeit nicht mehr gebraucht wurden
	 * werden abgebaut und die Ressourcen freigegeben. 
	 */
	@Override
	public void run() {
		if (LogSettings.enabled)
			Log.v(LOGTAG,"PrintSpooler background thread starts now.");
		
		while(_running) {
			try {
				if (LogSettings.enabled)
					Log.v(LOGTAG,"PrintSpooler enters sleep("+DESTINATION_QUEUE_CLEANUP_INTERVAL_MS+")");
				synchronized (_thread) {
					Thread.currentThread().wait(DESTINATION_QUEUE_CLEANUP_INTERVAL_MS);
				}
				if (LogSettings.enabled)
					Log.v(LOGTAG,"PrintSpooler sleep done.");
			} catch (InterruptedException silent) {}
			
			try {
				if (LogSettings.enabled)
					Log.v(LOGTAG, "PrintSpooler checks for pending prits in all open queues");
				checkPendingPrintsInQueues();
			} catch (Exception e) {
				if (LogSettings.enabled)
					Log.d(LOGTAG, e.toString());
			}
			
			try {
				if (LogSettings.enabled)
					Log.v(LOGTAG, "PrintSpooler checks destination queue finalzation for obsolete queues.");
				checkDestinationQueuesFinalization(false); //kein forcedClose, sondern abh�ngig davon ob die queue noch etiketten beinhaltet
			} catch (Exception e) {
				if (LogSettings.enabled)
					Log.d(LOGTAG, e.toString());
			}

		}
		
		//Beim Beenden des PrintSpoolers m�ssen alle allf�llig noch offenen
		//Druck-Warteschlangen ebenfalls geschlossen werden
		checkDestinationQueuesFinalization(true);
		if (LogSettings.enabled)
			Log.v(LOGTAG,"PrintSpooler background thread ends now.");
	}

	/**
	 * Diese Methode überprüft periodisch die registrierten
	 * Destinationen und solche welche über eine bestimmte Zeitspanne
	 * keinen Druckauftrag mehr hatten werden finalisiert.
	 * Die Objekte werden freigegeben und der BackgroundThread
	 * geschlossen.
	 */
	private void checkDestinationQueuesFinalization(boolean forcedClose) {
		List<String> closeQueueList=new ArrayList<String>();
		Set<Entry<String, Destination>> destSet = _destinationMap.entrySet();
		if (LogSettings.enabled)
			Log.v(LOGTAG,"closeDestinationQueues(). check for long lived not anymore used queues");
		
		for (Entry<String, Destination> entry : destSet) {
			Destination dest=entry.getValue();

			if (!forcedClose) {
				int cnt=dest.getLabelQueue().size();
				Date lastPrintTs=dest.getLastSuccessfulPrintTs();
				Date currTs=new Date();
				if (cnt<1) {
					long diff=currTs.getTime()-lastPrintTs.getTime();
					if (diff>QUEUE_MAX_LIVETIME_MS) {
						 closeQueueList.add(entry.getKey());
					}
				}
			} else //forcedClose erzwingt die Schliessung der Warteschlange
				closeQueueList.add(entry.getKey());
			
		}
		
		//markierte Queues abbauen
		for (String destKey : closeQueueList) {
			Destination dest=(Destination)_destinationMap.get(destKey);
			if (LogSettings.enabled)
				Log.v(LOGTAG,"closeQueue(). dest="+dest);
			dest.close();
			_destinationMap.remove(destKey);
		}
	}

	/**
	 * Löscht alle Druckaufträge für alle Destinationen
	 * in allen Queues.
	 */
	public void resetDestinationQueues() {
		Set<Entry<String, Destination>> destSet = _destinationMap.entrySet();
		if (LogSettings.enabled)
			Log.v(LOGTAG,"resetDestinationQueues(). reset all destnation queues.");
		for (Entry<String, Destination> entry : destSet) {
			Destination dest=entry.getValue();
			resetDestiationQueue(dest.getPrinterCharacteristics().getNetworkName(), dest.getPrinterCharacteristics().getPort());
		}		
	}
	
	/**
	 * Löscht die Queue der angegebenen Destination 
	 * @param host
	 * @param port
	 */
	public void resetDestiationQueue(String host, int port) {
		Destination dest=(Destination)_destinationMap.get(destKeyFor(host,port));
		if (dest!=null)
			dest.getLabelQueue().clear();
	}
	
	/**
	 * Diese Methode überprüft die aktuellen Druckqueues
	 * und generiert einen allfälligen callback für
	 * den Fiori Client
	 */
	private void checkPendingPrintsInQueues() {
		Set<Entry<String, Destination>> destSet = _destinationMap.entrySet();
		LinkedList<Object> linkedList=new LinkedList<Object>();
		
		if (LogSettings.enabled)
			Log.v(LOGTAG,"checkQueues()");
		
		int cntTotal=0;
		for (Entry<String, Destination> entry : destSet) {
			Destination dest=entry.getValue();
			int cnt=dest.getLabelQueue().size();
			if (LogSettings.enabled)
				Log.v(LOGTAG,"dest: "+ dest + ", queueSize="+cnt);
			
			//sind noch Etiketten in dieser Destination/Queue vorhanden?
			if (cnt>0) {
				Date lastCheckTs=dest.getLastQueueCheckTs();
				Date currTs=new Date();
				long diff=currTs.getTime()-lastCheckTs.getTime();
				if (diff>QUEUE_CHECK_INTERVAL_TS) {
					linkedList.add(dest.getPrinterCharacteristics().getNetworkName());
					linkedList.add(cnt);
					cntTotal+=cnt;
				}
			}
		}
		
		//Die zusammengefügte message hat dieses Format:
		//Array: [Total,<anz>,hostname/<anz>,....
		linkedList.addFirst(cntTotal);
		linkedList.addFirst("Total");
		
		if (cntTotal>0)
 			sendCordovaCallbackMessage(PluginResult.Status.OK, PRINTER_PLUGIN_CALLBACK_PENDING_PRINT_JOBS, linkedList);
			
	}
	
	/**
	 * Sendet den übergebenen Text und die Objektarray zum Cordova Container als
	 * Callback zurück. 
	 * 
	 * Ist der msgText nicht <null> wird es an array-position 0 gesetzt. Die Objektarray wird als JSON-Array angehängt.
	 * 
	 * @param status
	 * @param msgText
	 */
	private void sendCordovaCallbackMessage(final PluginResult.Status status, final String msgText, List<Object> values) {
		if (_cordovaInterface==null || _cordovaCallbackContext  == null)
			return;
		
		final JSONArray resultArr=new JSONArray();
		if (msgText!=null)
			resultArr.put(msgText);
		
		if (values!=null) {
			for (Object o : values) {
				resultArr.put(o);	
			}
		}
		
		_cordovaInterface.getActivity().runOnUiThread(new Runnable() {
			public void run() {
				PluginResult result = new PluginResult(status, resultArr);
				result.setKeepCallback(true);
				_cordovaCallbackContext.sendPluginResult(result);
			}
		});
	}
    

	/**
	 * Gibt die PrintSpooler instance zurück.
	 * @return
	 */
	public static PrintSpooler getInstance() {
		return s_instance;
	}

	/**
	 * Gibt eine Etikette an der i-Index Stelle der Queue zurück.
	 * Die Etikette wird nicht von der Queue entfernt.
	 * @param i
	 * @return Die Etikette am index i der aktuellen Queue oder null wenn aufgrund der Eingabe keine Etikette gefunden wird.
	 */
	public LabelDef getLabelDefAt(int i) {
		Destination dest=getCurrentDest();
		try {
			LinkedBlockingQueue<LabelDef> o=(LinkedBlockingQueue<LabelDef>)dest.getLabelQueue();
			Object[] a=o.toArray();
			return (LabelDef)a[i];
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * Startet den nativen PrintSpooler.
	 * Diese Funktion wird beim Setzen eines 
	 * Druckers automatisch aufgerufen.
	 */
	public void start() {
		synchronized(s_instance) {
			//Starte den spooler nur 1x. Wenn der spooler bereits
			//l�uft, dann muss dieser erst mit stop() gestoppt werden
			//bevor wieder ein start() m�glich ist.
			if (_running)
				return;
			_running=true;
		}
		
		_thread=new Thread(this);
		_thread.setName("PrintSpooler");
		_thread.start();
		
	}
	
	/**
	 * Stoppt den nativen Printspooler und
	 * löscht alle noch nicht gedruckten Etiketten
	 * aus allen Destinationen/Queues
	 */
	public void stop() {
		resetDestinationQueues();
		checkDestinationQueuesFinalization(true);
		
		synchronized(s_instance) {
			_running=false;
		}
		synchronized(_thread) {
			_thread.notify();
		}
	}

	/**
	 * Erhält die Statusänderungen aller momentan aktiven Desinationen.
	 * Es werden aber nur die Änderungen für den aktuell gesetzten
	 * Drucker dem Fiori Gui weitergegeben.
	 * Dieser kann die Information nutzen um z.B. eine Statusinformation
	 * für den Benutzer aufzubereiten.
	 */
	@Override
	public void stateChanged(Destination dest, PrintDestStateE printState, PrintLookupStateE lookupState) {
		if (_currPrinter!=null) { //ist ein current printer gesetzt?
			if (_currPrinter.getNetworkName().equalsIgnoreCase(dest.getPrinterCharacteristics().getNetworkName())) {
				List<Object> list = new ArrayList<Object>();
				list.add(dest.getPrinterCharacteristics().getNetworkName());
				list.add(printState.ordinal());
				list.add(printState);
				list.add(lookupState.ordinal());
				list.add(lookupState);
				sendCordovaCallbackMessage(PluginResult.Status.OK, PRINTER_PLUGIN_CALLBACK_STATE_CHANGE, list);
			}
		}
	}

}
