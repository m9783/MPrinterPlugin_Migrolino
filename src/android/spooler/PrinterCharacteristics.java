/*
* Migros-Genossenschafts-Bund
*/
package ch.migros.plugin.printer.spooler;

/**
 * Die PrinterCharacteristics hält die Informationen
 * zu einem Drucker. 
 *
 * @author FMaffeni
 */
public class PrinterCharacteristics {
	/**
	 * @return the networkName
	 */
	public String getNetworkName() {
		return _networkName;
	}
	/**
	 * @param networkName the networkName to set
	 */
	public void setNetworkName(String networkName) {
		_networkName = networkName;
	}
	
	/**
	 * @return the port
	 */
	public int getPort() {
		return _port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		_port = port;
	}
	/**
	 * @return the printerModel
	 */
	public String getPrinterModel() {
		return _printerModel;
	}
	/**
	 * @param printerModel the printerModel to set
	 */
	public void setPrinterModel(String printerModel) {
		_printerModel = printerModel;
	}
	/**
	 * @return the printLanguage
	 */
	public PrintLanguageE getPrintLanguage() {
		return _printLanguage;
	}
	/**
	 * @param printLanguage the printLanguage to set
	 */
	public void setPrintLanguage(PrintLanguageE printLanguage) {
		_printLanguage = printLanguage;
	}

	private final static String NOT_AVAILABLE="n/a";
	private String _networkName=NOT_AVAILABLE;
	private int _port=6101;
	private String _printerModel=NOT_AVAILABLE;
	private PrintLanguageE _printLanguage=PrintLanguageE.undefined;
	
}
