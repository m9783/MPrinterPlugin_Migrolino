/*
* Migros-Genossenschafts-Bund
*/
package ch.migros.plugin.printer.spooler;

/**
 * Dieses Enum hält alle zZt. bekannten, bzw.
 * durch die Fiori Applikation zu unterst�tzenden
 * Drucksprachen.
 *
 * @author FMaffeni
 */
public enum PrintLanguageE {
	undefined
	,ZPL
	,CPCL
}
