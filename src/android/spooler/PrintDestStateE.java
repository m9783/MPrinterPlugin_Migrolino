/*
* Migros-Genossenschafts-Bund
*/
package ch.migros.plugin.printer.spooler;

/**
 * Dieser Enum beschreibt die Zustände
 * einer Druckdestination 
 *
 * @author FMaffeni
 */
public enum PrintDestStateE {
	 undefined
	,startPending
	,started
	,stopped
	,idle
	,printing
}
