/*
 * Migros-Genossenschafts-Bund
 */
package ch.migros.plugin.printer.spooler;

import java.util.Date;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import android.util.Log;
import ch.migros.plugin.printer.LogSettings;
import ch.migros.plugin.printer.network.Connection;
import ch.migros.plugin.printer.stats.Statistics;

/**
 * Eine Destination hält die Information zum Drucker, die Netzwerkverbindung,
 * die Etiketten-Queue und das Warteschlagen-Thread welches für das Senden
 * der Etiketten zuständig ist. Pro Drucker wird ein Destination Objekt instanziert.
 * 
 *
 * @author FMaffeni
 */
public class Destination implements Runnable {
	private static final String LOGTAG = Destination.class.getName();

	private Queue<LabelDef>        	_labelQueue=new LinkedBlockingQueue<LabelDef>();
	private PrinterCharacteristics 	_printerCharacteristics;
	private Connection             	_connection=null;
	private volatile Thread     	_printerQueueThread=null;
	private volatile Thread 		_printerLookupThread=null;
	private Destination.PrinterLookup _printerLookup=null;
	private boolean 				_running=false;
	private static final long       JOIN_WAIT_INTERVAL_LONG=15000;
	private static final long       JOIN_WAIT_INTERVAL_SHORT=500;
	private Date					_lastSuccessfulPrintTs=new Date();
	private Date					_lastQueueCheckTs=new Date();
	private volatile PrintDestStateE    _currPrintState=PrintDestStateE.undefined;
	private DestinationStateChangeListenerI  _stateChangeListener = null;

	private PrintLookupStateE _currLookupState=PrintLookupStateE.undefined;
	
	/**
	 * Contsructor
	 * @param pc PrinterCharacteristics mit Netzwerkadressse, Portnummer, Druckersprache
	 */
	public Destination(PrinterCharacteristics pc, DestinationStateChangeListenerI stateChangeListener) {
		_printerCharacteristics=pc;
		_connection=new Connection(pc.getNetworkName(), pc.getPort());
		_stateChangeListener =stateChangeListener;
		_running=true;
		notifyPrinterStateChange(PrintDestStateE.startPending);
		
		//Dieser Thread �berpr�ft ob der Drucker erreichbar ist
		_printerLookup=new PrinterLookup(this);
		_printerLookupThread=new Thread(new PrinterLookup(this));
		_printerLookupThread.setName("Destination (PrinterLookup) " +pc.getNetworkName() + ":" + pc.getPort());
		_printerLookupThread.start();

		//Dieser Thread verwaltet die Druckerwarteschlange
		_printerQueueThread=new Thread(this);
		_printerQueueThread.setName("Destination (PrinterQueue) " +pc.getNetworkName() + ":" + pc.getPort());
		_printerQueueThread.start();
	}

	/**
	 * @param printing
	 */
	private void notifyPrinterStateChange(PrintDestStateE newPrintState) {
		notifyPrinterStateListener(newPrintState, null);
	}
	/**
	 * @param printing
	 */
	private void notifyLookupStateChange(PrintLookupStateE newLookupState) {
		notifyPrinterStateListener(null,newLookupState);
	}

	/**
	 * Gibt den Statuswechsel einer Destination dem PrintSpooler weiter.
	 */
	private void notifyPrinterStateListener(PrintDestStateE newPrintState, PrintLookupStateE newLookupState) {
		try {
			boolean ps = newPrintState == null || _currPrintState == newPrintState;
			boolean ls = newLookupState == null || _currLookupState == newLookupState;

			//checken ob mindestens einer der beiden Stati gewechselt hat, sonst event nicht weitergeben
			if (ls && ps)
				return;

			if (newPrintState != null)
				_currPrintState = newPrintState;

			if (newLookupState != null)
				_currLookupState = newLookupState;

			if (_stateChangeListener != null)
				_stateChangeListener.stateChanged(this, _currPrintState, _currLookupState);
		} catch (Exception ex) {
			Log.e(LOGTAG,"notifyPrinterStateListener() Ex:" +ex.toString(),ex);
		}
	}

	/**
	 * @return the printerCharacteristics
	 */
	public PrinterCharacteristics getPrinterCharacteristics() {
		return _printerCharacteristics;
	}

	/**
	 * @param printerCharacteristics the printerCharacteristics to set
	 */
	public void setPrinterCharacteristics(
			PrinterCharacteristics printerCharacteristics) {
		_printerCharacteristics = printerCharacteristics;
	}

	/**
	 * Gibt die an dieser Destination gebundene Etiketten-Queue zur�ck
	 * @return the labelQueue
	 */
	public Queue<LabelDef> getLabelQueue() {
		return _labelQueue;
	}

	/**
	 * Den Warteschlagenthread benachrichtigen, dass eine oder mehrere
	 * Etiketten in die Queue gelegt wurden
	 */
	public void fireAddEvent() {
		if (_running) {
			if (LogSettings.enabled)
				Log.v(LOGTAG,"fireAddEvent(). before entering synchronized block");
			synchronized(_printerQueueThread) {
				_printerQueueThread.notify();
				if (LogSettings.enabled)
					Log.v(LOGTAG,"fireAddEvent(). synchronized block. bg-thread notified");
			}
		}
		else
			if (LogSettings.enabled)
				Log.i("Destination.fireAddEvent()", "Event not fired. Background-Thread for Destination is not running (anymore).");
	}

	/**
	 * Warteschlangenthread. Die in der Queue verfügbaren Etiketten
	 * werden hier verarbeitet und der gehaltenen Connection �bergeben
	 */
	@Override
	public void run() {

		int copies=0;
		Connection.RetVal ret=new Connection.RetVal();
		if (LogSettings.enabled)
			Log.v(LOGTAG,"Destination background thread is running. " +_printerQueueThread.getName());
		while (_running) {
			try {
				if (_labelQueue.size()>0) {
					if (LogSettings.enabled)
						Log.v(LOGTAG,"Destination queue check() cnt="+_labelQueue.size() + " " +_printerQueueThread.getName());

					if (!_connection.isSending()) {
						LabelDef labelDef = _labelQueue.peek();
						copies=labelDef.getCopiesToPrint();
						_connection.addToPrintBuffer(labelDef.getPrinterCommand(),copies);
						if (LogSettings.enabled)
							Log.v(LOGTAG,"Destination background thread added label with copies="+copies
									+" for printing to the connection buffer. " 
									+_printerQueueThread.getName());
						
						//nur Printing Status weiterleiten, wenn der Drucker gefunden wurde da sonst
						//Falschmeldungen weitergeleitet werden.
						if (_currLookupState.equals(PrintLookupStateE.dnsLookupOkPingOk))
							notifyPrinterStateChange(PrintDestStateE.printing);
						
						ret=_connection.send(labelDef.getAnswerExcepted(),labelDef.getWaitMs());

						//callback
						Destination.PrinterResponseI asyncResponseCallback=labelDef.getAsyncResponseCallback();
						boolean wasOk=ret.ok;
						if (labelDef.getAnswerExcepted()) {
							if (asyncResponseCallback!=null)
								asyncResponseCallback.responseReceived(ret);

							//Für Druckaufträge, welche eine Antwort erwarten,muss in jedem Fall ret.ok auf true gesetzt werden.
							//Dies auch wenn der Ausdruck nicht möglich war. Der Grund ist, dass die App eine Antwort (ok oder nok)
							//innerhalb des gesetzten Timeouts erwartet. Wenn z.B. der Drucker ausgeschaltet ist, bringt eine
							//verspätete Antwort keinen Nutzen mehr! (Achtung: wird ok nicht auf true gesetzt, löst der callback
							//eine schnelle wiederholte abfolge von Rückmessages in Richtung App aus!
							wasOk=true;
						}

						if (wasOk) {//wenn der Durck ok war, die Etikette definitiv von der Queue entfernen
							_labelQueue.poll();
							notifyPrinterStateChange(PrintDestStateE.idle);
							if (LogSettings.enabled)
								Log.v(LOGTAG,"Destination background remove label from queue. " +_printerQueueThread.getName());
							_lastSuccessfulPrintTs=new Date();
							Statistics.incPrinted(_connection.getHost(), copies);
						}
					} else {
						if (LogSettings.enabled)
							Log.v(LOGTAG,"Destination background: connection is already sending. wait for next send cycle. " +_printerQueueThread.getName());
					}
				}
			} catch (Exception e) {
				Statistics.incFailed(_connection.getHost(), copies);
				if (LogSettings.enabled)
					Log.e(this.getClass().getName(), "Exception caught on printing Label. "+e.toString(),e);
			}

			notifyPrinterStateChange(PrintDestStateE.idle);

			try {
				//wenn die letzte Etikette ohne Fehler gesendet wurde und noch 
				//weitere anstehen, dann sofort die n�chste Etikette abarbeiten
				if (ret.ok && _labelQueue.size()>0)
					continue;

				//sonst: wenn keine Etiketten in der queue sind den Lang Interval verwenden
				//       sind noch Etiketten in der Queue welche aber durch den Fehler nicht
				//       gedruckt werden konnten, dann den short interval nehmen.
				long waitJoinMs=_labelQueue.size() >0 
						? JOIN_WAIT_INTERVAL_SHORT
								: JOIN_WAIT_INTERVAL_LONG;

				if (LogSettings.enabled)
					Log.v(LOGTAG,"Destination background WAIT("+waitJoinMs+" ms)" + _printerQueueThread.getName());
				synchronized (_printerQueueThread) {
					_printerQueueThread.wait(waitJoinMs);					
				}

			} catch (InterruptedException silent) {
				if (LogSettings.enabled)
					Log.v(LOGTAG,"Destination background join interrupted. " + _printerQueueThread.getName());
			}
			if (LogSettings.enabled)
				Log.v(LOGTAG,"Destination background WAIT DONE. " +_printerQueueThread.getName());

		}

		if (LogSettings.enabled)
			Log.v(LOGTAG,"Destination background thread is ending. " +_printerQueueThread.getName());

	}


	/**
	 * Gibt den letzten Zeitstempel eines erfolgreichen Drucks
	 * auf dieser Destination zurück.
	 * @return the lastSuccessfulPrintTs
	 */
	public Date getLastSuccessfulPrintTs() {
		return _lastSuccessfulPrintTs;
	}

	/**
	 * Gibt den Zeitstempel des letzten Queue Check zur�ck
	 * @return the lastQueueCheckTs
	 */
	public Date getLastQueueCheckTs() {
		return _lastQueueCheckTs;
	}

	/**
	 * Setzt den Zeitstempel des letzten Queue Checks
	 * @param lastQueueCheckTs the lastQueueCheckTs to set
	 */
	public void setLastQueueCheckTs(Date lastQueueCheckTs) {
		_lastQueueCheckTs = lastQueueCheckTs;
	}

	/**
	 * Schliesst diese Destination und die gehaltene
	 * Netzwerkverbindung. Der Warteschlangenthread wird
	 * ebenfalls beendet.
	 */
	public void close() {
		_running=false;
		_connection.close();
		_printerLookup.close();
		synchronized(_printerQueueThread) {
			_printerQueueThread.notify();
		}
		notifyPrinterStateChange(PrintDestStateE.stopped);
	}

	/**
	 * @return the currPrintState
	 */
	public PrintDestStateE getCurrPrintState() {
		return _currPrintState;
	}

	private class PrinterLookup implements Runnable {
		private Destination _dest=null;
		boolean _isResolved=false;
		boolean _isReached=false;
		
		PrinterLookup(Destination dest) {
			_dest=dest;
		}
		
		@Override
		public void run() {
			int waitJoinMs=1500;
			PrintLookupStateE newLookupState=PrintLookupStateE.undefined;
			
			while (_running) {
				try {
					_isResolved=_connection.isHostNameResolved();
					_isReached=_isResolved
							  ?_connection.ping()
							  :false;
					if (LogSettings.enabled)
						Log.v(LOGTAG,"PrinterLookup nsLookup for host="+_connection.getHost() +", nsLookup=" +_isResolved + ", ping="+_isReached);
					synchronized (_printerLookupThread) {
						_printerLookupThread.wait(waitJoinMs);					
					}
				} catch (InterruptedException silent) {
					if (LogSettings.enabled)
						Log.v(LOGTAG,"Destination background join interrupted. " + _printerQueueThread.getName());
				}
				
				if (_isResolved && _isReached)
					newLookupState=PrintLookupStateE.dnsLookupOkPingOk;

				else if (_isResolved && !_isReached)
					newLookupState=PrintLookupStateE.dnsLookupOkPingFailed;

				else
					newLookupState=PrintLookupStateE.dnsLookupFailed;

				_dest.notifyLookupStateChange(newLookupState);
			}
		}
		
		public void close() {
			_running=false;
			synchronized (_printerLookupThread) {
				_printerLookupThread.notify();					
			}
		}
	}

	/**
	 * Diese Schnittstelle muss implementiert werden, um die Drucker Antwort
	 * Asynchron zu erhalten. Das Callbackobjekt muss dem send() Befehl mitgegeben werden.
	 */
	public interface PrinterResponseI {
		public void responseReceived(Connection.RetVal printerResponse);
	}
}