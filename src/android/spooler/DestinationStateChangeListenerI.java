/*
* Migros-Genossenschafts-Bund
*/
package ch.migros.plugin.printer.spooler;

/**
 * @author FMaffeni
 */
public interface DestinationStateChangeListenerI {
	void stateChanged(Destination dest, PrintDestStateE state, PrintLookupStateE lookupState);
}
