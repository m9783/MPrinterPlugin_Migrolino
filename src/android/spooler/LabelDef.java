/*
* Migros-Genossenschafts-Bund
*/
package ch.migros.plugin.printer.spooler;

import java.util.Date;

/**
 * Die LabelDef hält die Druckdefinition (Druckbefehle)
 * in der Druckersprache für eine Etikette. 
 * Zusätzlich werden noch folgende Informationen gehalten.
 * 
 * - Zeitpunkt des Druckauftrages
 * - Anzahl zu druckende Kopien. Anzahl <1 wird automatisch auf 1 gesetzt
 * - DruckerCharakteristik Objekt für den adressierten Drucker
 * @see PrinterCharacteristics
 * 
 * @author FMaffeni
 */
public class LabelDef {
	/**
	 * @return the copiesToPrint
	 */
	public int getCopiesToPrint() {
		return _copiesToPrint;
	}

	/**
	 * @return Should the printconnection wait for an answer from the printer? true/false
     */
	public boolean getAnswerExcepted() {
		return _answerExcepted;
	}

	/**
	 * @return how long to wait for an answer in mills.
     */
	public int getWaitMs() {
		return _waitForAnswerMs;
	}

	/**
	 * @return the Callbackobject for the async response from the printer
	 */
	public Destination.PrinterResponseI getAsyncResponseCallback() {
		return _asyncResponseCallback;
	}


	/**
	 * @param copiesToPrint the copiesToPrint to set
	 */
	public void setCopiesToPrint(int copiesToPrint) {
		_copiesToPrint = copiesToPrint;
	}

	private Date _labelCreatedTs;
	private PrinterCharacteristics _destPrinter;
	private byte[] _printerCommand;
	private int _copiesToPrint=1;
	private boolean _answerExcepted=false;
	private int     _waitForAnswerMs=0;
	private Destination.PrinterResponseI _asyncResponseCallback=null;
	
	/**
	 * @return the labelCreatedTs
	 */
	public Date getLabelCreatedTs() {
		return _labelCreatedTs;
	}

	/**
	 * @param labelCreatedTs the labelCreatedTs to set
	 */
	public void setLabelCreatedTs(Date labelCreatedTs) {
		_labelCreatedTs = labelCreatedTs;
	}

	/**
	 * @return the destPrinter
	 */
	public PrinterCharacteristics getDestPrinter() {
		return _destPrinter;
	}

	/**
	 * @param destPrinter the destPrinter to set
	 */
	public void setDestPrinter(PrinterCharacteristics destPrinter) {
		_destPrinter = destPrinter;
	}

	/**
	 * @return the printerCommand
	 */
	public byte[] getPrinterCommand() {
		return _printerCommand;
	}

	/**
	 * @param printerCommand the printerCommand to set
	 */
	public void setPrinterCommand(byte[] printerCommand) {
		_printerCommand = printerCommand;
	}

	public LabelDef(PrinterCharacteristics pc, byte[] printCommand, int copies, boolean answerExcepted, int waitForAnswerMs,Destination.PrinterResponseI asyncResponseCallback) {
		_labelCreatedTs=new Date();
		_destPrinter=pc;
		_printerCommand=printCommand;
		_copiesToPrint=copies>0 ? copies : 1;
		_answerExcepted=answerExcepted;
		_waitForAnswerMs=waitForAnswerMs;
		_asyncResponseCallback=asyncResponseCallback;
	}
}
