/*global cordova, module*/

module.exports = {

    printLabel: function(successCallback, errorCallback, label, quantity, answerExcepted, waitForAnswerMs) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "printLabel", [label, quantity, answerExcepted, waitForAnswerMs]);
    },

    setCurrentPrinter: function(successCallback, errorCallback, hostName, portNum) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "setCurrentPrinter", [hostName, portNum]);
    },

    getPrinterLanguage: function(successCallback, errorCallback, enumVal, enumText) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "getPrinterLanguage", [enumVal, enumText]);
    },

    stopPrintSpooler: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "stopPrintSpooler", []);
    },

    startPrintSpooler: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "startPrintSpooler", []);
    },

    resetAllPrintQueues: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "resetAllPrintQueues", []);
    },

    resetPrintQueueForDest: function(successCallback, errorCallback, hostName, portNum) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "resetPrintQueueForDest", [hostName, portNum]);
    },

    registerProcessingCallback: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "registerProcessingCallback", []);
    },

    unregisterProcessingCallback: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "unregisterProcessingCallback", []);
    },

    setLogging: function(successCallback, errorCallback, int) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "setLogging", [int]);
    },

    getCurrentPrinter: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "getCurrentPrinter", []);
    },

    removeCurrentPrinter: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "MPrinterPlugin", "removeCurrentPrinter", []);
    },
};
