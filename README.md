# migros-plugin-printer

This plugin provides an API for printing with Zebra QLn220 mobile printers.

## Installation

    cordova plugin add migros-plugin-printer --searchpath /path/to/folder/containing/plugin-repo

### Description

Migros uses this plugin to print `Spezpreis`, `Einzeletiketten` and `Preisetiketten` labels. 

__NOTE__: In order to scan the `Connect Printer` labels, install __migros-plugin-scanner__ plugin.

### Supported Platforms

- Android

### Example

not yet documented